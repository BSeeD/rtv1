# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jmontene <jmontene@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/04/11 14:12:16 by jmontene          #+#    #+#              #
#    Updated: 2017/11/07 18:57:47 by jmontene         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc

CFLAGS = -Wall -Wextra -O2 ##-fsanitize=address -g3 ##-Werror

NAME = rtv1

_CFILE = main.c tools.c sdlinit.c intersect.c draw.c \
		mousebindings.c debug.c keybindings.c matrix.c matrix2.c \
		vectormaths.c translate.c \

LIBDIR = ./libft

LIB_HEADERS = -I libft/includes

LIB_LINK = -L libft/ -lft

CDIR = ./srcs

CFILE = $(patsubst %, $(CDIR)/%, $(_CFILE))

OFILE = $(CFILE:.c=.o)

AUXDIR = $(LIBDIR)

HEADERS = includes

SDL2_PATHS = -rpath @loader_path/frameworks

SDL2_HEADERS = -I frameworks/SDL2.framework/Headers -I frameworks/SDL2_image.framework/Headers -I frameworks/SDL2_ttf.framework/Headers

SDL2_FWORKS = -framework SDL2 -framework SDL2_image -framework SDL2_ttf -F ./frameworks

.PHONY: libft clean fclean re all

all: libft $(NAME)

$(NAME): $(OFILE)
	@echo "Creation Executable $(NAME)............"
	@$(CC) -o $@ $^ $(CFLAGS) -I $(HEADERS) $(LIB_HEADERS) $(LIB_LINK) $(SDL2_PATHS) $(SDL2_FWORKS)
	@clear
	@echo "\033[1;32m   ___ _                         "
	@echo "  | _ |_)_ _  __ _ _ _ _  _      "
	@echo "  | _ \ | ' \/ _\` | '_| || |     "
	@echo "  |___/_|_||_\__,_|_|  \_, |     "
	@echo "     \033[42m| _ \_   _|\033[40m       |__/      "
	@echo "     \033[42m|   / | |  \033[40m                 "
	@echo "   __\033[42m|_|_\ |_|  \033[40m    _          _ "
	@echo "  / __|_ _ ___ __ _| |_ ___ __| |"
	@echo " | (__| '_/ -_) _\` |  _/ -_) _\` |"
	@echo "  \___|_| \___\__,_|\__\___\__,_|"
	@echo "  October 2017        by Jmontene \033[0m"

%.o: %.c
	@echo "Making $@............"
	@$(CC) -o $@ -c $< $(CFLAGS) -I $(HEADERS) $(LIB_HEADERS) $(SDL2_HEADERS)
	@echo "DONE"
	@echo " "

libft:
	@clear
	@echo "Making LibFT............"
	@make -C $(LIBDIR)
	@echo "DONE"
	@echo " "

clean:
	@clear
	@echo "Removing Files.........."
	@echo " "
	@echo " "
	@make -C $(LIBDIR) clean
	@echo "LibFT : DONE"
	@echo " "
	@rm -rf $(OFILE)
	@echo "$(OFILE) : DONE"
	@echo " "
	@sleep 0.1

fclean: clean
	@make -C $(LIBDIR) fclean
	@echo "libft.a : DONE"
	@echo " "
	@rm -rf $(NAME)
	@echo "$(NAME) : DONE"
	@clear
	@echo "\033[1;32m                                                           "
	@echo "                      _/_/_/    _/_/_/_/_/                           "
	@echo "                     _/    _/      _/                                "
	@echo "                    _/_/_/        _/                                 "
	@echo "                   _/    _/      _/                                  "
	@echo "                  _/    _/      _/                                   "
	@echo "                                                                     "
	@echo "                                                                     "
	@echo "    _/_/_/              _/              _/                      _/   "
	@echo "   _/    _/    _/_/    _/    _/_/    _/_/_/_/    _/_/      _/_/_/    "
	@echo "  _/    _/  _/_/_/_/  _/  _/_/_/_/    _/      _/_/_/_/  _/    _/     "
	@echo " _/    _/  _/        _/  _/          _/      _/        _/    _/      "
	@echo "_/_/_/      _/_/_/  _/    _/_/_/      _/_/    _/_/_/    _/_/_/       "
	@echo "                                                                     "
	@echo "                                                       \033[40m      "

re: fclean all
