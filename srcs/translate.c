/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   translate.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 17:36:03 by jmontene          #+#    #+#             */
/*   Updated: 2017/11/07 17:36:08 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

float	*gettransmatrix(t_v3f trans)
{
	float *matrix;

	if (!(matrix = malloc(sizeof(*matrix) * 16)))
		return (0);
	initmatrix(matrix);
	matrix[3] = trans.x;
	matrix[7] = trans.y;
	matrix[11] = trans.z;
	return (matrix);
}

void	translatematrix(float *matrix, int t)
{
	float *transmatrix;
	t_v3f translation;

	if (t == TRANSLATE_FORWARD)
		translation = vassign(0,0,-1);
	else if (t == TRANSLATE_BACKWARDS)
		translation = vassign(0,0,1);
	else
		translation = vassign(0,0,0);
	transmatrix = gettransmatrix(translation);
	multmatrix(matrix, transmatrix);
	free (transmatrix);
}
