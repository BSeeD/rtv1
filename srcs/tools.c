/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/23 15:04:28 by jmontene          #+#    #+#             */
/*   Updated: 2017/10/23 15:04:36 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	swapt(t_poly2 *poly)
{
	float temp;

	temp = poly->t0;
	poly->t0 = poly->t1;
	poly->t1 = temp;
}

void	respoly2(t_poly2 *poly)
{
	float	q;

	poly->delta = poly->b * poly->b - 4 * poly->a * poly->c;
	if (poly->delta == 0)
	{
		poly->t0 = -0.5 * poly->b / poly->a;
		poly->t1 = poly->t0;
	}
	else
	{
		q = (poly->b > 0) ?
		-0.5 * (poly->b + sqrt(poly->delta)) :
		-0.5 * (poly->b - sqrt(poly->delta)) ;
		poly->t0 = q / poly->a;
		poly->t1 = poly->c / q;
	}
	if (poly->t0 > poly->t1)
		swapt(poly);
}
