/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 15:37:16 by jmontene          #+#    #+#             */
/*   Updated: 2017/11/07 15:37:28 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

float	*initvec4(t_v3f *ray)
{
	float *temp;

	if (!(temp = malloc(sizeof(*temp) * 4)))
		exit (1);
	temp[0] = ray->x;
	temp[1] = ray->y;
	temp[2] = ray->z;
	temp[3] = 1;

	return (temp);
}

float	*initout()
{
	int i;
	float *temp;

	if (!(temp = malloc(sizeof(*temp) * 4)))
		exit (0);
	i = 0;
	while (i < 4)
	{
		temp[i] = 0.;
		i++;
	}
	return (temp);
}

void applysum(float *out, t_v3f *ray)
{
	ray->x = out[0];
	ray->y = out[1];
	ray->z = out[2];
}

void applymatrix(t_v3f *ray, float *matrix)
{
	float *vec4;
	float *out;
	int i;
	int j;

	vec4 = initvec4(ray);
	out = initout();
	i = 0;
	while (i < 4)
	{
		j = 0;
		while (j < 4)
		{
			out[i] += matrix[i * 4 + j] * vec4[j];
			j++;
		}
		i++;
	}
	applysum(out, ray);
	free(vec4);
	free(out);
}

void	multmatrix(float *matrix, float *rot)
{
	float out[16];
	float sum;
	int i;
	int j;
	int k;

	i = -1;
	while (++i < 4)
	{
		j = -1;
		while (++j < 4)
		{
			sum = 0;
			k = -1;
			while (++k < 4)
				sum += rot[i * 4 + k] * matrix[k * 4 + j];
			printf(">%f< ", out[i * 4 + j] = sum);
		}
		printf("\n");
	}
	//printf("\n\n--------------------\n\n");
	printf("\n\n");
	copymatrix(&out, matrix);
}
