/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   debug.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 10:54:32 by jmontene          #+#    #+#             */
/*   Updated: 2017/11/02 10:54:39 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	debugcoord(t_env *env, t_ray ray, t_coord screen)
{
	if ((env->debugpos.on == true)
		&& (env->debugpos.pos.x == screen.x)
		&& (env->debugpos.pos.y == screen.y))
		{
			printf("x >%f< y >%f< z >%f<\n", ray.dir.x, ray.dir.y, ray.dir.z);
			env->debugpos.inter = true;
		}
/*	if ((env->debugpos.on == true)
		&& (screen.x == (SCREEN_WIDTH/2))
		&& (screen.y == (SCREEN_HEIGHT/2)))
		printf("x >%f< y >%f< z >%f<\n", ray.dir.x, ray.dir.y, ray.dir.z);*/
}

void	debugpos(t_debugpos *debug, SDL_Event *event)
{
	if ((event->motion.x > 0) && (event->motion.y > 0))
	{
		debug->pos.x = event->motion.x;
		debug->pos.y = event->motion.y;
		ft_printf("x >%d< y >%d<\n", event->motion.x, event->motion.y);
		debug->on = true;
	}
}

void	debugintersphere(t_debugpos *debug, t_ray ray,
		t_sphere sphere, t_poly2 poly)
{
	if (debug->inter == true)
	{
		printf("delta >%f< t0 >%f< t1 >%f<\n", poly.delta, poly.t0, poly.t1);
		debug->inter = false;
	}
}
