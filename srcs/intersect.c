/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   intersect.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/25 12:07:08 by jmontene          #+#    #+#             */
/*   Updated: 2017/10/25 12:07:16 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

int		intersphere(t_debugpos *debug, t_ray *ray, t_sphere *s)
{
	t_poly2 poly;
	t_v3f	dist;

	poly.a = vdot(&ray->dir, &ray->dir);
	dist = vsub(&ray->pos, &s->pos);
	poly.b = 2. * vdot(&ray->dir, &dist);
	poly.c = vdot(&dist, &dist) - (s->radius * s->radius);
	respoly2(&poly);
	debugintersphere(debug, *ray, *s, poly);
	ray->t0 = poly.t0;
	if (poly.delta < 0.)
		return (0);
	if (poly.t0 < 0.)
	{
		poly.t0 = poly.t1;
		ray->t0 = poly.t0;
		if (poly.t0 < 0.)
			return (0);
	}
	debugintersphere(debug, *ray, *s, poly);
	return (1);
}
