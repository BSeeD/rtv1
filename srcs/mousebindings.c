/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mousebindings.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 10:50:06 by jmontene          #+#    #+#             */
/*   Updated: 2017/11/02 10:50:12 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	mousebindings(t_env *env)
{
	if (env->event.button.button == SDL_BUTTON_LEFT)
		debugpos(&env->debugpos, &env->event);
}
