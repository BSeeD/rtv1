/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sdlinit.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/25 12:05:15 by jmontene          #+#    #+#             */
/*   Updated: 2017/10/25 12:05:24 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

t_argbmask	byteorder(void)
{
	t_argbmask result;

	if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
	{
		result.r = 0xFF000000;
		result.g = 0x00FF0000;
		result.b = 0x0000FF00;
		result.a = 0x000000FF;
	}
	else
	{
		result.r = 0x00FF0000;
		result.g = 0x0000FF00;
		result.b = 0x000000FF;
		result.a = 0xFF000000;
	}
	return (result);
}

int		sdlinit(t_env *env)
{
	t_argbmask mask;

	mask = byteorder();
	if (SDL_Init(SDL_INIT_EVERYTHING))
		ft_printf("SDL init error\nError number : %s\n", SDL_GetError());
	else if (!(env->win = SDL_CreateWindow("RTV1",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		SCREEN_WIDTH, SCREEN_HEIGHT, 0)))
		ft_printf("Window creation error\nError number : %s\n", SDL_GetError());
	else if (!(env->buffer = SDL_CreateRGBSurface(0,
		SCREEN_WIDTH, SCREEN_HEIGHT, 32,
		mask.r, mask.g, mask.b, mask.a)))
		ft_printf("Surface init error\nError number : %s\n", SDL_GetError());
	else
		return (0);
	exit(0);
}
