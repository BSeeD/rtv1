/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/22 17:42:36 by jmontene          #+#    #+#             */
/*   Updated: 2017/10/27 10:34:01 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void initmatrix(float *matrix)
{
	int i;
	int j;

	i = 0;
	while (i < 4)
	{
		j = 0;
		while (j < 4)
		{
			matrix[i * 4 + j] = 0;
			j++;
		}
		i++;
	}
	matrix[0 * 4 + 0] = 1;
	matrix[1 * 4 + 1] = 1;
	matrix[2 * 4 + 2] = 1;
	matrix[3 * 4 + 3] = 1;
}


void	init(t_env *env, float *matrix)
{
	env->quit = false;
	env->debugpos.on = false;
	env->debugpos.inter = false;
	initmatrix(matrix);
}

void		loop(t_env *env, float *matrix, t_sphere *s)
{
	while (SDL_PollEvent(&env->event) != 0)
	{
		if (env->event.type == SDL_QUIT)
			env->quit = true;
		else if (env->event.type == SDL_KEYDOWN)
		 	keybindings(env, matrix);
		else if (env->event.type == SDL_MOUSEBUTTONDOWN)
			mousebindings(env);
	}
	drawstuff(env, matrix, s);
}

int			main(int ac, char **av)
{
	t_env *env;
	t_sphere s;
	float *matrix;

	if (!(env = malloc(sizeof(*env))))
		return (0);
	if (!(matrix = malloc(sizeof(*matrix) * 16)))
		return (0);
	//s.color = uinttoargb(0xFFFF2535);
	init(env, matrix);
	s.pos = vassign(0., -10., -200.);
	s.radius = 20.;
	sdlinit(env);
	while (!env->quit)
		loop(env, matrix, &s);
}
