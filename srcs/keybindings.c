/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keybindings.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 10:50:06 by jmontene          #+#    #+#             */
/*   Updated: 2017/11/02 10:50:12 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	keybindings(t_env *env, float *matrix)
{
	if (env->event.key.keysym.sym == SDLK_ESCAPE)
		env->quit = true;
	else if (env->event.key.keysym.sym == SDLK_w)
		translatematrix(matrix, TRANSLATE_FORWARD);
	else if (env->event.key.keysym.sym == SDLK_s)
		translatematrix(matrix, TRANSLATE_BACKWARDS);
}
