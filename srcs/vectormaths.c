/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vectormaths.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/23 09:55:30 by jmontene          #+#    #+#             */
/*   Updated: 2017/10/23 09:55:39 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

t_v3f   vassign(float x, float y, float z)
{
    t_v3f result;

    result.x = x;
    result.y = y;
    result.z = z;
    return (result);
}

t_v3f   vsub(t_v3f *v1, t_v3f *v2)
{
    t_v3f result = {v1->x - v2->x, v1->y - v2->y, v1->z - v2->z};
    return (result);
}

float   vdot(t_v3f *v1, t_v3f *v2)
{
    return(v1->x * v2->x + v1->y * v2->y + v1->z * v2->z);
}

void    normalize(t_v3f *v)
{
    float len2 = vdot(v, v);
    // avoid division by 0
    if (len2 > 0)
    {
        float invLen = 1 / sqrt(len2);
        v->x *= invLen;
        v->y *= invLen;
        v->z *= invLen;
    }
}
