/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/25 12:08:56 by jmontene          #+#    #+#             */
/*   Updated: 2017/10/25 12:09:01 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"
Uint32 getcolor(Uint32 color, float t0)
{
	t_color	color_rgb;

	color_rgb.argb.a = color & 0xFF000000;
	color_rgb.argb.r = color & 0x00FF0000;
	color_rgb.argb.g = color & 0x0000FF00;
	color_rgb.argb.b = color & 0x000000FF;
	color_rgb.argb.a =  (Uint32)((float)color_rgb.argb.a / (t0));
	color_rgb.argb.r =  (Uint32)((float)color_rgb.argb.r / (t0));
	color_rgb.argb.g =  (Uint32)((float)color_rgb.argb.g / (t0));
	color_rgb.argb.b =  (Uint32)((float)color_rgb.argb.b / (t0));
	return (color_rgb.pixel);
}

Uint32		resolvecolor(t_debugpos *debug, t_ray *ray, t_sphere *s)
{
	Uint32 color;
	/*if ((debugpos.on == true)
		&& (debugpos.pos.x == screen.x)
		&& (debugpos.pos.y == screen.y))
		printf("x >%f< y >%f< z >%f<\n", ray->dir.x, ray->dir.y, ray->dir.z);*/
	if (intersphere(debug, ray, s))
		color =  getcolor(0xFFFF0000, ray->t0);
	else
		color = 0x00000000;
	return (color);
}

void		drawstuff(t_env *env, float *matrix, t_sphere *s)
{
	t_coord	screen;
	Uint32	*pixels;
	t_ray	ray;
	float	imageAspectRatio;

	SDL_FillRect(env->buffer, NULL, 0x000000);
	ray.pos = vassign(0., 0., 0.);
	applymatrix(&ray.pos, matrix);
	ray.dir.z = -1.;
	pixels = env->buffer->pixels;
	screen.y = 0;
	imageAspectRatio = (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT; // assuming width > height
	while (screen.y < SCREEN_HEIGHT)
	{
		screen.x = 0;
		ray.dir.y = (1. - 2. * ((float)screen.y + 0.5) / (float)SCREEN_HEIGHT) * tan(FOV / 2.);
		while (screen.x < SCREEN_WIDTH)
		{
			ray.dir.x = (2. * ((float)screen.x + 0.5) / (float)SCREEN_WIDTH - 1.) * tan(FOV / 2.) * imageAspectRatio;
			applymatrix(&ray.dir, matrix);
			normalize(&ray.dir);
			debugcoord(env, ray, screen);
			pixels[screen.y * SCREEN_WIDTH + screen.x] = resolvecolor(&env->debugpos, &ray, s);
			screen.x++;
		}
		screen.y++;
	}
	env->surf = SDL_GetWindowSurface(env->win);
	SDL_BlitSurface(env->buffer, NULL, env->surf, NULL);
	SDL_UpdateWindowSurface(env->win);
	env->debugpos.on = false;

}
