/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_float.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 07:06:07 by jmontene          #+#    #+#             */
/*   Updated: 2017/11/04 07:06:17 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_printf_float(t_hd *hd, va_list *ap)
{
	char	*s;
	int		len;

	(void)hd;
	s = (ft_ftoa_base((double)va_arg(*ap, double), 4, 10, 0));
	len = ft_strlen(s);
	if (len == 0)
		return (0);
	write(1, s, len);
	return (len);
}
