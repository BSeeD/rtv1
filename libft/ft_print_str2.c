/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_str.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 17:52:43 by jmontene          #+#    #+#             */
/*   Updated: 2017/03/12 18:12:39 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		pre_padding_str(t_hd *hd, int fcount)
{
	int count;

	count = 0;
	if ((!hd->flags.bits.prec) || (!fcount))
		hd->pvalue = fcount;
	else if (hd->pvalue > fcount)
		hd->pvalue = fcount;
	if (!hd->flags.bits.minus)
	{
		while (hd->pvalue < hd->wvalue)
		{
			write(1, " ", 1);
			hd->wvalue--;
			count++;
		}
	}
	return (count);
}

int		write_str(t_hd *hd)
{
	if (!hd->pvalue)
		return (0);
	write(1, hd->out, hd->pvalue);
	hd->wvalue -= hd->pvalue;
	return (hd->pvalue);
}

int		post_padding_str(t_hd *hd)
{
	int count;

	count = 0;
	while (hd->wvalue > 0)
	{
		write(1, " ", 1);
		hd->wvalue--;
		count++;
	}
	return (count);
}
