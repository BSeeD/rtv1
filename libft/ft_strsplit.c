/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/11 10:57:20 by jmontene          #+#    #+#             */
/*   Updated: 2016/11/16 17:02:15 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static int	ft_count_word(const char *str, char c)
{
	int		word_number;
	int		b;

	word_number = 0;
	b = 0;
	while (str[b] != '\0')
	{
		if (str[b] == c)
			b++;
		else
		{
			while ((str[b] != c) && (str[b] != '\0'))
				b++;
			word_number = word_number + 1;
		}
	}
	return (word_number);
}

static int	ft(const char *str, int b, char c)
{
	int		letter_count;

	letter_count = 0;
	while ((str[b + letter_count] != c) && (str[b + letter_count] != '\0'))
		letter_count++;
	return (letter_count);
}

char		**ft_strsplit(const char *str, char c)
{
	char	**t;
	int		b;
	int		word;
	int		i;

	b = 0;
	word = -1;
	if (!(str))
		return (0);
	if (!(t = (char**)(malloc(sizeof(char*) * ((ft_count_word(str, c) + 1))))))
		return (0);
	while (++word < ft_count_word(str, c))
	{
		i = -1;
		while (str[b] == c)
			b++;
		t[word] = ft_strsub(str, b, ft(str, b, c));
		b = b + ft(str, b, c) + 1;
	}
	if (!(t[word] = (char*)malloc(sizeof(char) * 1)))
		return (0);
	t[word] = 0;
	return (t);
}
