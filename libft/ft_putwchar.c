/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putwchar.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/09 11:43:13 by jmontene          #+#    #+#             */
/*   Updated: 2017/03/13 11:11:21 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <unistd.h>

static int	wide(wchar_t chr, int i)
{
	if (i == 4)
	{
		ft_putchar((chr >> 18) + 0xF0);
		ft_putchar(((chr >> 12) & 0x3F) + 0x80);
		ft_putchar(((chr >> 6) & 0x3F) + 0x80);
		ft_putchar((chr & 0x3F) + 0x80);
	}
	if (i == 3)
	{
		ft_putchar((chr >> 12) + 0xE0);
		ft_putchar(((chr >> 6) & 0x3F) + 0x80);
		ft_putchar((chr & 0x3F) + 0x80);
	}
	if (i == 2)
	{
		ft_putchar((chr >> 6) + 0xC0);
		ft_putchar((chr & 0x3F) + 0x80);
	}
	return (i);
}

int			ft_putwchar(wchar_t chr)
{
	int i;

	i = 0;
	if ((MB_CUR_MAX == 1 && chr <= 0xFF) || (chr <= 0x7F))
		write(1, &chr, ++i);
	else if (MB_CUR_MAX == 1 && chr > 0xFF)
		i = 0;
	else if (chr <= 0x7FF)
		i = 2;
	else if (chr <= 0xFFFF)
		i = 3;
	else if (chr <= 0x10FFFF)
		i = 4;
	return ((i > 1) ? wide(chr, i) : i);
}
