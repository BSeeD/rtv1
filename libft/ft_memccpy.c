/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/08 12:45:25 by jmontene          #+#    #+#             */
/*   Updated: 2016/11/09 14:57:25 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	int				i;
	unsigned char	*tempsrc;
	unsigned char	*tempdst;

	tempsrc = (unsigned char *)src;
	tempdst = (unsigned char *)dst;
	i = 0;
	while (n)
	{
		if ((tempdst[i] = tempsrc[i]) == (unsigned char)c)
			return (&tempdst[i + 1]);
		i++;
		n--;
	}
	return (0);
}
