/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_str.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/11 17:52:43 by jmontene          #+#    #+#             */
/*   Updated: 2017/03/12 18:18:29 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int				ft_printf_str(t_hd *hd, va_list *ap)
{
	int count;

	count = 0;
	if ((hd->out = va_arg(*ap, char*)))
	{
		count += pre_padding_str(hd, ft_strlen(hd->out));
		count += write_str(hd);
		count += post_padding_str(hd);
	}
	else
		count += ft_write_null();
	return (count);
}

static int		ft_handle_wstr(t_hd *hd, wchar_t *s, int i, int j)
{
	int count;

	count = 0;
	if (hd->flags.bits.prec && (hd->pvalue < ft_wstrlen(s)))
	{
		while ((i += ft_wcharlen(*(s + j))) <= hd->pvalue)
			j++;
		i -= ft_wcharlen(*(s + j - 1));
	}
	else
		i = ft_wstrlen(s);
	count += pre_padding_str(hd, i);
	j = 0;
	if (hd->pvalue > 0)
	{
		while (((j += ft_wcharlen(*s)) <= hd->pvalue) && (*s))
			count += ft_putwchar(*s++);
		hd->wvalue -= hd->pvalue;
	}
	count += post_padding_str(hd);
	return (count);
}

int				ft_printf_wstr(t_hd *hd, va_list *ap)
{
	wchar_t	*s;
	int		i;
	int		j;
	int		count;

	i = 0;
	j = 0;
	count = 0;
	if ((s = va_arg(*ap, wchar_t*)))
		count += ft_handle_wstr(hd, s, i, j);
	else
		count += ft_write_null();
	return (count);
}
