/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/10 16:10:51 by jmontene          #+#    #+#             */
/*   Updated: 2016/11/14 15:01:48 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char		*ft_strtrim(char const *s)
{
	size_t	i;
	size_t	j;

	i = 0;
	if (s)
	{
		j = ft_strlen(s) - 1;
		while (s[i] == ' ' || s[i] == '\n' || s[i] == '\t')
			i++;
		while ((s[j] == ' ' || s[j] == '\n' || s[j] == '\t') && j)
			j--;
		if (!j && i)
			return (ft_strdup(""));
		if (i <= j)
			return (ft_strsub(s, i, j - i + 1));
	}
	return (0);
}
