/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/07 14:26:21 by jmontene          #+#    #+#             */
/*   Updated: 2016/11/09 14:42:56 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	int		i;
	int		last;
	char	*temps;

	i = 0;
	last = -1;
	temps = (char *)s;
	while (temps[i] != '\0')
	{
		if (temps[i] == c)
			last = i;
		i++;
	}
	if (last >= 0)
		return (&temps[last]);
	else
		return ((c == '\0') ? &temps[i] : 0);
}
