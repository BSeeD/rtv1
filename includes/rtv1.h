/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rtv1.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmontene <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/22 17:50:43 by jmontene          #+#    #+#             */
/*   Updated: 2017/10/23 09:58:03 by jmontene         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RTV1_H
# define RTV1_H

# include <SDL.h>
# include <SDL_image.h>
# include <SDL_ttf.h>
//# include <time.h>
# include <math.h>
# include "libft.h"

# define SCREEN_WIDTH 640
# define SCREEN_HEIGHT 480
# define FOV 0.73

typedef	enum 		e_bool
{
	false,
	true
}					t_bool;

enum				e_translate
{
	TRANSLATE_FORWARD,
	TRANSLATE_BACKWARDS,
	TRANSLATE_LEFT,
	TRANSLATE_RIGHT,
	TRANSLATE_UP,
	TRANSLATE_DOWN,
	TRANSLATE_TOTAL
};

typedef struct		s_v3f
{
	float			x;
	float			y;
	float			z;
}					t_v3f;

typedef struct		s_argb
{
	int				a;
	int				r;
	int				g;
	int				b;
}					t_argb;

typedef struct		s_sphere
{
	t_v3f			pos;
	float			radius;
//	t_argb			color;
}					t_sphere;

typedef struct		s_ray
{
	t_v3f			pos;
	t_v3f			dir;
	float			t0;
}					t_ray;

typedef struct		s_poly2
{
	float			a;
	float			b;
	float			c;
	float			delta;
	float			t0;
	float			t1;
}					t_poly2;

typedef struct		s_coord
{
	int				x;
	int				y;
}					t_coord;

typedef struct		s_debugpos
{
	t_coord			pos;
	t_bool			on;
	t_bool			inter;
}					t_debugpos;

typedef struct		s_env
{
	SDL_Window		*win;
	SDL_Surface		*surf;
	SDL_Surface		*buffer;
	SDL_Event		event;
	t_debugpos		debugpos;
	t_bool			quit;
}					t_env;

typedef struct		s_argbmask
{
	Uint32			a;
	Uint32			r;
	Uint32			g;
	Uint32			b;
}					t_argbmask;

typedef union       u_color
{
	t_argb			argb;
	Uint32			pixel;
}                   t_color;

t_v3f				vassign(float x, float y, float z);
t_argb				cassign(float a, float x, float y, float z);
t_v3f				vsub(t_v3f *v1, t_v3f *v2);
float				vdot(t_v3f *v1, t_v3f *v2);
void				respoly2(t_poly2 *poly);
int					intersphere(t_debugpos *debug, t_ray *ray, t_sphere *s);
void				drawstuff(t_env *env, float *matrix, t_sphere *s);
int					sdlinit(t_env *env);
void				mousebindings(t_env *env);
void				keybindings(t_env *env, float *matrix);
void				normalize(t_v3f *v);
void				debugpos(t_debugpos *debug, SDL_Event *event);
void				debugcoord(t_env *env, t_ray ray, t_coord screen);
void				debugintersphere(t_debugpos *debug, t_ray ray,
	t_sphere sphere, t_poly2 poly);
void				initmatrix(float *matrix);
void				applymatrix(t_v3f *ray, float *matrix);
void				multmatrix(float *matrix, float *rot);
void				translatematrix(float *matrix, int t);
void				copymatrix(float *source, float *dest);


#endif
